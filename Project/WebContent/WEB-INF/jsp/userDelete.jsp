<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザ削除確認</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/userUpdateStyle.css">
</head>

<body>
	<header>
		<li class="navbar-text">${userInfo.name}さん</li> <a class="logout"
			href="/usermanagement/LogoutServlet">ログアウト</a>
	</header>

	<div class="container">

	<h1>ユーザ削除確認</h1>

		<p>本当に ${id} を消去しますか？</p>

		<div>
			<div class="row">
				<div class="col-md-2">
					<a href="/usermanagement/UserListServlet" class="btn btn-danger">キャンセル</a>
				</div>
				<div class="col-md-2">
					<form action="/usermanagement/UserDeleteServlet" method="post">
						<button type="submit" class="btn btn-danger">はい</button>
						<input type="hidden" name="loginId" value="${id}">
					</form>
				</div>
			</div>
		</div>
	</div>
</body>
</html>