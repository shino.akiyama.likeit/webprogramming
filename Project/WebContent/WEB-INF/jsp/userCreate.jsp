<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザ新規登録</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/userCreateStyle.css">
</head>
<body>
	<header>
		<li class="navbar-text">${userInfo.name}さん</li> <a class="logout"
			href="/usermanagement/LogoutServlet">ログアウト</a>
	</header>

	<div class="container">

		<h1>ユーザ新規登録</h1>

		<c:if test="${errMsg != null}">
			<div class="alert alert-danger" role="alert">${errMsg}</div>
		</c:if>

		<form action="/usermanagement/UserCreateServlet" method="post">
			<div class="form-group row">
				<label for="loginId" class="col-sm-2 col-form-label">ログインID</label>
				<div class="col-sm-10">
					<input type="text" name="loginId" class="form-control" id="loginId"
						placeholder="ログインID">
				</div>
			</div>
			<div class="form-group row">
				<label for="inputPassword3" class="col-sm-2 col-form-label">パスワード</label>
				<div class="col-sm-10">
					<input type="password" name="password" class="form-control"
						id="inputPassword3" placeholder="パスワード">
				</div>
			</div>
			<div class="form-group row">
				<label for="inputPassword3" class="col-sm-2 col-form-label">パスワード（確認用）</label>
				<div class="col-sm-10">
					<input type="password" name="passwordCheck" class="form-control"
						id="inputPassword3" placeholder="パスワード（確認用）">
				</div>
			</div>

			<div class="form-group row">
				<label for="userName" class="col-sm-2 col-form-label">ユーザ名</label>
				<div class="col-sm-10">
					<input type="text" name="name" class="form-control" id="userName"
						placeholder="ユーザ名">
				</div>
			</div>
			<div class="form-group row">
				<label for="birthDate" class="col-sm-2 col-form-label">生年月日</label>
				<div class="col-sm-10">
					<input type="date" name="birthDate" class="form-control"
						id="birthDate" max="9999-12-31">
				</div>
			</div>
			<div class="form-group row">
				<div class="col-sm-12 end-btn">
					<button type="submit" class="btn btn-primary">登録</button>
				</div>
			</div>
		</form>

		<a href="/usermanagement/UserListServlet" class="return">戻る</a>

	</div>
</body>
</html>