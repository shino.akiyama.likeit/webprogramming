<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>


<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ログイン画面</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/indexStyle.css">
</head>


<body>

	<header>
	<div class="container">
		<a class="navbar-brand" href="index.html">ユーザ管理システム</a>
	</div>
	</header>

	<div class="container mt-5">
		<c:if test="${errMsg != null}">
			<div class="alert alert-danger" role="alert">${errMsg}</div>
		</c:if>


		<form class="form-signin" action="LoginServlet" method="post">

			<div class="form-group">
				<input type="text" name="loginId" id="inputLoginId"
					class="form-control" placeholder="ログインID">
			</div>
			<div class="form-group">
				<input type="password" name="password" id="inputPassword"
					class="form-control" placeholder="パスワード">
			</div>
			<button type="submit" class="btn btn-primary">ログイン</button>
		</form>

	</div>
</body>
</html>