<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta"UTF-8">
<title>ユーザ一覧画面</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/userListStyle.css">
</head>

<body>
	<header>
		<li class="navbar-text">${userInfo.name}さん</li> <a class="logout"
			href="/usermanagement/LogoutServlet">ログアウト</a>
	</header>

	<div class="container mt-5">

		<c:if test="${userInfo.loginId == 'admin' }">
			<div class="create-button">
				<a class="btn btn-secondary"
					href="/usermanagement/UserCreateServlet">新規登録</a>
			</div>
		</c:if>

		<form action="/usermanagement/UserListServlet" method="post">
			<div class="form-group row">
				<label for="loginId" class="col-sm-2 col-form-label">ログインID</label>
				<div class="col-sm-10">
					<input type="text" name="loginId" class="form-control" id="loginId"
						placeholder="ログインID">
				</div>
			</div>
			<div class="form-group row">
				<label for="userName" class="col-sm-2 col-form-label">ユーザ名</label>
				<div class="col-sm-10">
					<input type="password" name="name" class="form-control" id="name"
						placeholder="ユーザ名">
				</div>
			</div>
			<div class="form-group row">
				<label for="birthDate" class="col-sm-2 col-form-label">生年月日</label>
				<div class="row col-sm-10">
					<div class="col-sm-5">
						<input type="date" name="startBirthDate" id="startBirthDate"
							class="form-control" />
					</div>
					<div class="col-sm-1 text-center">~</div>
					<div class="col-sm-5">
						<input type="date" name="endBirthDate" id="endBirthDate"
							class="form-control" />
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="btn-a">
					<button type="submit" class="btn btn-primary">検索</button>
				</div>
			</div>
		</form>

		<div class="table-responsive">
			<table class="table table-striped">
				<thead>
					<tr>
						<th>ログインID</th>
						<th>ユーザ名</th>
						<th>生年月日</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="user" items="${userList}">
						<tr>
							<td>${user.loginId}</td>
							<td>${user.name}</td>
							<td>${user.birthDate}</td>
							<td><a class="btn btn-secondary"
								href="UserDetailServlet?id=${user.loginId}">詳細</a> <c:if
									test="${userInfo.loginId == 'admin' || userInfo.loginId == user.loginId}">
									<a class="btn btn-info"
										href="UserUpdateServlet?id=${user.loginId}">更新</a>
								</c:if> <c:if test="${userInfo.loginId == 'admin' }">
									<a class="btn btn-danger"
										href="UserDeleteServlet?id=${user.loginId}">削除</a>
								</c:if></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
</body>
</html>