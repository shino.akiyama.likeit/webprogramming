<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザ詳細画面</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/userDetailStyle.css">
</head>
<body>
	<header>
		<li class="navbar-text">${userInfo.name}さん</li> <a class="logout"
			href="/usermanagement/LogoutServlet">ログアウト</a>
	</header>

	<div class="container">

	<h1>ユーザ情報詳細</h1>

		<div class="row inf-tnk">
			<div class="col-sm-4">ログインID</div>
			<div class="col-sm-8">${user.id}</div>
		</div>

		<div class="row inf-tnk">
			<div class="col-sm-4">ユーザ名</div>
			<div class="col-sm-8">${user.name}</div>
		</div>

		<div class="row inf-tnk">
			<div class="col-sm-4">生年月日</div>
			<div class="col-sm-8">${user.birthDate}</div>
		</div>

		<div class="row inf-tnk">
			<div class="col-sm-4">新規登録日時</div>
			<div class="col-sm-8">${user.createDate}</div>
		</div>

		<div class="row inf-tnk">
			<div class="col-sm-4">更新日時</div>
			<div class="col-sm-8">${user.updateDate}</div>
		</div>

		<a href="/usermanagement/UserListServlet" class="return">戻る</a>

	</div>
</body>
</html>