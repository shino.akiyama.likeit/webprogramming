package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserUpdateServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();

		if(session.getAttribute("userInfo")==null) {
			response.sendRedirect("/usermanagement/LoginServlet");
			return;
		}
		String loginId = request.getParameter("id");

		UserDao userDao = new UserDao();
		User user = userDao.UserInfo(loginId);

		request.setAttribute("user", user);
		request.setAttribute("errMsg", session.getAttribute("errMsg"));
		// セッション削除
		session.removeAttribute("errMsg");

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();

		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String passwordCheck = request.getParameter("passwordCheck");
		String name = request.getParameter("name");
		String birthDate = request.getParameter("birthDate");

		UserDao userDao = new UserDao();

		if (!(password.equals(passwordCheck))) {
			// リクエストスコープにエラーメッセージをセット
			session.setAttribute("errMsg", "入力された内容は正しくありません");
			// リダイレクト
			response.sendRedirect("/usermanagement/UserUpdateServlet?id=" + loginId);
			return;
		} else if ("".equals(name) || "".equals(birthDate)) {
			// リクエストスコープにエラーメッセージをセット
			session.setAttribute("errMsg", "入力された内容は正しくありません");
			// リダイレクト
			response.sendRedirect("UserUpdateServlet?id=" + loginId);
			return;
		}

		if ("".equals(password) || "".equals(passwordCheck)) {
			userDao.upDateUserInfoTwo(loginId, name, birthDate);
			response.sendRedirect("/usermanagement/UserListServlet");
		} else {
			userDao.upDateUserInfo(loginId, name, password, birthDate);
			response.sendRedirect("/usermanagement/UserListServlet");
		}
	}
}
